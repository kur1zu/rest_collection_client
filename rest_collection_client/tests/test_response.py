from itertools import repeat

from pandas import DataFrame

from rest_collection_client.response import RestCollectionDeserializeMap, \
    RestCollectionResponse


class _DeserializerFactory(object):

    def __init__(self, model_name):
        self.model_name = model_name

    def __call__(self, data):
        return tuple(
            {
                '%s_id' % self.model_name: int(item['id']),
                '%s_name' % self.model_name: str(item['name'])
            } for item in data
        )


_DESERIALIZE_MAP = RestCollectionDeserializeMap(
    a=_DeserializerFactory('a'),
    b=_DeserializerFactory('b'),
    c=_DeserializerFactory('c'),
)


_RAW_RESPONSE = {
    'a': [
        {'id': '1', 'name': 123}
    ],
    'b': [
        {'id': '2', 'name': 321},
        {'id': '3', 'name': 'test'},
    ]
}


def test_deserialize():
    # Test for simple deserialization.
    deserialized_a = (
        {'a_id': 1, 'a_name': '123'},
    )

    assert _DESERIALIZE_MAP.deserialize(_RAW_RESPONSE['a'], 'a') == \
        deserialized_a

    # Test for raw rest collection response deserialization.
    deserialized_response = {
        'a': deserialized_a,
        'b': (
            {'b_id': 2, 'b_name': '321'},
            {'b_id': 3, 'b_name': 'test'},
        )
    }

    assert _DESERIALIZE_MAP.deserialize_response(_RAW_RESPONSE) == \
        deserialized_response

    # Test for chunks deserialization.
    assert _DESERIALIZE_MAP.deserialize_chunked_response(
        tuple(repeat(_RAW_RESPONSE, 2))
    ) == tuple(repeat(deserialized_response, 2))


def test_response():
    # Test for initialization from raw_response
    response = RestCollectionResponse.from_raw_response(_RAW_RESPONSE)
    assert response['a'].equals(
        DataFrame(_RAW_RESPONSE['a'], dtype=object)
    )
    assert response['b'].equals(
        DataFrame(_RAW_RESPONSE['b'], dtype=object)
    )

    # Test for deserialization.
    response = RestCollectionResponse.deserialize(
        _RAW_RESPONSE, _DESERIALIZE_MAP
    )
    assert response['a'].equals(
        DataFrame([{'a_id': 1, 'a_name': '123'}], dtype=object)
    )

    # Chunks test.
    chunk = _RAW_RESPONSE.copy()
    del chunk['a']
    chunk['b'] = [chunk['b'][1], {'id': 2, 'name': '2'}]

    chunk['c'] = [{'id': 10, 'name': 10}]

    response = RestCollectionResponse.deserialize_chunked(
        (_RAW_RESPONSE, chunk), _DESERIALIZE_MAP
    )

    assert response['b'].equals(
        DataFrame([
            {'b_id': 2, 'b_name': '321'},
            {'b_id': 3, 'b_name': 'test'},
            {'b_id': 2, 'b_name': '2'}
        ], dtype=object)
    )
    assert response['a'].equals(
        DataFrame([{'a_id': 1, 'a_name': '123'}], dtype=object)
    )
    assert response['c'].equals(
        DataFrame([{'c_id': 10, 'c_name': '10'}], dtype=object)
    )
