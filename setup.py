from setuptools import find_packages, setup

setup(
    name='rest_collection_client',
    version='0.4.0',
    packages=find_packages(exclude=['rest_collection_client.tests*']),
    url='https://bitbucket.org/p-app/rest_collection_client',
    license='MIT',
    author='Vladimir V. Pivovarov',
    author_email='admin@p-app.ru',
    description='rest_collection library client',
    python_requires='>=3.6',
    install_requires=[
        'aiohttp',
        'ujson',
        'pandas',
        'extra_collections<1.0.0',
    ],
    extras_require={
        'dev': [
            'pytest',
            'pip-tools',
        ]
    }
)
